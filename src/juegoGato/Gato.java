package juegoGato;
import java.util.Random;
/**
 *
 * @author Gio Arellano
 */
public class Gato {
    char[][] gato;
    char turno;
    int disponibles;
    public Gato(char turno){
        this.gato = new char[3][3];
        this.turno = turno;
        disponibles = 9;
    }
    public char getTurno(){
        return turno;
    }
    public void changeTurno(){
        if(turno == 'X')
            this.turno = 'O';
        else
            this.turno = 'X';
    }
    public void tirar(int f, int c){
        gato[f][c] = turno;
        disponibles--;
        System.out.println(turno+""+disponibles);
    }
    public boolean comprobar(){
        boolean band = false;
        for(int i = 0; i < 3; i++){
            boolean band2 = true;
            for(int j = 0; j < 3; j++)
                if(gato[i][j] != turno)band2=false;
            if(band2)
                return band = true;
        }
        for(int i = 0; i < 3; i++){
            boolean band2 = true;
            for(int j = 0; j < 3; j++)
                if(gato[j][i] != turno)band2=false;
            if(band2)
                return band = true;
        }
        if(gato[0][0] == turno && gato[1][1] == gato[0][0] && gato[2][2] == gato[0][0]){
            band = true;
        }else if(gato[0][2] == turno && gato[1][1] == gato[0][2] && gato[2][2] == gato[0][2]){
           band = true; 
        }
        return band;
    }
    public int bot(){
        int h = 0, v = 0;
        int celda = 0;
        while(true){
            if(disponibles == 1){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        if(gato[i][j] != 'X' && gato[i][j] != 'O'){
                            h = i;
                            v = j;
                            gato[h][v] = 'O';
                            break;
                         }
                    }
                }
            }
            Random random= new Random();
            h = (int)(random.nextDouble()*3);
            v = (int)(random.nextDouble()*3);
            System.out.println("---\n "+ h + " "+v);
            if(gato[h][v]!='X' && gato[h][v]!='O'){
                System.out.println(gato[h][v]+"");
                gato[h][v] = 'O';
                break;
                //band = false;
            }
        }
        celda = obtenerCelda(h, v);
        disponibles--;
        System.out.println(celda);
        return celda;
    }
    public int obtenerCelda(int h, int v){
        if(h==0 && v == 0){
            return 1;
        }else if(h==0 && v == 1){
            return 2;
        }else if(h==0 && v == 2){
            return 3;
        }else if(h==1 && v == 0){
            return 4;
        }else if(h==1 && v == 1){
            return 5;
        }else if(h==1 && v == 2){
            return 6;
        }else if(h == 2 && v == 0){
            return 7;
        }else if(h==2 && v == 1){
            return 8;
        }else{
            return 9;
        }
    }
    
}
