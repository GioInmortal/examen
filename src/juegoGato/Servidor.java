/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegoGato;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
/**
 *
 * @author Gio Arellano
 */
public class Servidor {
    ArrayList<Conexion> conexiones;
    ServerSocket ss;
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        });
    }
    private void start() {
        this.conexiones = new ArrayList<>();
        Socket socket;
        Conexion cnx;
        try {
            ss = new ServerSocket(4444);
            System.out.println("Servidor iniciado, en espera de conexiones");
            socket = ss.accept();
            cnx = new Conexion(this, socket, conexiones.size(), 'X', true);
            cnx.start();
            conexiones.add(cnx);
            socket = ss.accept();
            cnx = new Conexion(this, socket, conexiones.size(), 'o', false);
            cnx.start();
            conexiones.add(cnx);
            /*while (true){
                socket = ss.accept();
                if(cont == 1)
                    cnx = new Conexion(this, socket, conexiones.size(), 'X', false);
                else
                    cnx = new Conexion(this, socket, conexiones.size(), 'O', true);
                conexiones.add(cnx);
                cnx.start();
                if(cont == 2)
                    break;
            }*/
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Broadcasting
    public void iniciar(Conexion cnx, String id, String type, char letter, boolean turn) {
        JSONObject message = new JSONObject();
        message.put("type", type);
        message.put("letter", letter+"");
        message.put("turn", turn);
        System.out.println(message.toString());
        cnx.enviar(message.toString());
    }
    public void myTurn(String id, String type, int h, int v) {
        Conexion hilo;
        JSONObject message = new JSONObject();
        message.put("type", type);
        message.put("horizontal", h);
        message.put("vertical", v);
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected() && !id.equals(hilo.id)){
                hilo.enviar(message.toString());
            }
        }
    }
    class Conexion extends Thread {
        BufferedReader in;
        PrintWriter    out;
        Socket cnx;
        Servidor padre;
        int numCnx = -1;
        String id = "";
        char letter;
        boolean turn;
        public Conexion(Servidor padre, Socket socket, int num, char letter, boolean turn){
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+num;
            this.letter = letter;
            this.turn = turn;
        }

        @Override
        public void run() {
            String linea = "";
            JSONObject user, message;
            boolean ejecutar = true;
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                System.out.printf("Aceptando conexion desde %s\n", cnx.getInetAddress().getHostAddress());
                this.padre.iniciar(this, id, "start", letter, turn);
                while((linea = in.readLine()) != null){
                    message = new JSONObject(linea);
                    String type = message.getString("type");
                    if(type.equals("myTurn")){
                        this.padre.myTurn(this.id, type, message.getInt("horizontal"), message.getInt("vertical"));
                        System.out.println(message);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.
        }
    }
}